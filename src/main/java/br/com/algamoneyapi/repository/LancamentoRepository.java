package br.com.algamoneyapi.repository;

import java.time.LocalDate;
import java.util.List;

import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;

import br.com.algamoneyapi.model.Lancamento;

@Repository
public interface LancamentoRepository extends JpaRepository<Lancamento, Long> {

	@Query(value = "SELECT l FROM Lancamento l WHERE l.dataVencimento = :data")
	List<Lancamento> findByDataVencimento(@Param("data") LocalDate dataVencimento);

	@Query(value = "SELECT l FROM Lancamento l WHERE l.dataVencimento BETWEEN :de AND :ate")
	List<Lancamento> findByDataVencimentoBetween(@Param("de") LocalDate de, @Param("ate") LocalDate ate);

	@Query(value = "SELECT l FROM Lancamento l WHERE  l.descricao LIKE :descricao ")
	List<Lancamento> findByDescricaoContaining(@Param("descricao") String descricao);

	@Query(value = "SELECT l FROM Lancamento l WHERE  l.descricao LIKE :descricao OR l.dataVencimento BETWEEN :de AND :ate ")
	List<Lancamento> findByDescricaoContainingOrDataVencimentoBetween(@Param("descricao") String descricao,
			@Param("de") LocalDate de, @Param("ate") LocalDate ate);

	@Query(value = "SELECT l FROM Lancamento l")
	Page<Lancamento> findAll(Pageable pageable);

	@Query(value = "SELECT l.codigo, l.descricao, l.valor, l.pessoa.nome FROM Lancamento l "
			     + "WHERE l.descricao LIKE %:descricao% ")
	List<Object[]> pesquisarPelaDescricao(@Param("descricao") String descricao);
	
}
