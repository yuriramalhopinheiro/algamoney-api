package br.com.algamoneyapi.model;

import java.io.Serializable;

import javax.persistence.Embeddable;
import javax.validation.constraints.Size;

import org.hibernate.validator.constraints.NotEmpty;

@Embeddable
public class Endereco implements Serializable {

	private static final long serialVersionUID = 1L;

	@NotEmpty
	@Size(min = 1, max = 100)
	private String logradouro;

	@NotEmpty
	@Size(min = 1, max = 10)
	private String numero;

	@NotEmpty
	@Size(min = 3, max = 100)
	private String bairro;

	@NotEmpty
	@Size(min = 8, max = 8)
	private String cep;

	@NotEmpty
	@Size(min = 3, max = 150)
	private String cidade;

	@NotEmpty
	@Size(min = 3, max = 100)
	private String estado;
	
	@Size(max = 15)
	private String complemento;

	public Endereco() {
		super();
	}

	public String getLogradouro() {
		return logradouro;
	}

	public void setLogradouro(String logradouro) {
		this.logradouro = logradouro;
	}

	public String getNumero() {
		return numero;
	}

	public void setNumero(String numero) {
		this.numero = numero;
	}

	public String getBairro() {
		return bairro;
	}

	public void setBairro(String bairro) {
		this.bairro = bairro;
	}

	public String getCep() {
		return cep;
	}

	public void setCep(String cep) {
		this.cep = cep;
	}

	public String getCidade() {
		return cidade;
	}

	public void setCidade(String cidade) {
		this.cidade = cidade;
	}

	public String getEstado() {
		return estado;
	}

	public void setEstado(String estado) {
		this.estado = estado;
	}

	public String getComplemento() {
		return complemento;
	}

	public void setComplemento(String complemento) {
		this.complemento = complemento;
	}

}
