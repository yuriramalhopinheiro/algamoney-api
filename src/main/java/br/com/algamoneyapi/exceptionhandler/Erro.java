package br.com.algamoneyapi.exceptionhandler;

/**
 Recebe a mensagens de erro que seram enviadas 
 para o usuário ou desenvolvedor

*/
public class Erro {

	private String usuario;
	private String desenvolvedor;

	public Erro(String usuario, String desenvolvedor) {
		super();
		this.usuario = usuario;
		this.desenvolvedor = desenvolvedor;
	}

	public String getUsuario() {
		return usuario;
	}

	public String getDesenvolvedor() {
		return desenvolvedor;
	}

}