package br.com.algamoneyapi.exceptionhandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.http.converter.HttpMessageNotReadableException;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.algamoneyapi.exceptionhandler.factory.Personalizavel;

/**
	Envia uma mensagem de erro customizada para o usuário ou desenvolvedor quando a API
	receber uma requisção com valores inválidos.
	
	@see Personalizavel
	
	@author Yuri Ramalho pinheiro
 */
public class HttpMessageNotReadableExceptionHandler extends ResponseEntityExceptionHandler{
	
	@Autowired
	private Personalizavel personalizavel;
	
	@Override
	protected final ResponseEntity<Object> handleHttpMessageNotReadable(HttpMessageNotReadableException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		return this.personalizavel.criarResponseEntity("mensagem.invalida", ex, HttpStatus.BAD_REQUEST, request); 
	}
	
}
