package br.com.algamoneyapi.exceptionhandler.factory;

import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;

/**
 * @author Yuri Ramalho pinheiro
 * */

@Configuration
@ComponentScan(basePackages={"br.com.algamoneyapi"})
public class BeanFactory {
	
	/**
	 * @see ResponseEntityPersonalizada
	 * */
	@Bean @Primary
	public Personalizavel criarResponseEntityPersonalizavel() {
		
		return new ResponseEntityPersonalizada();
	}
}
