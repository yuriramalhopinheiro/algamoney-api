package br.com.algamoneyapi.exceptionhandler.factory;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;

/**
 * 
 * @author Yuri Ramalho Pinheiro
 * 
 * @see BeanFactory
 * */

public interface Personalizavel {
	
	/**
	 * Abastração para criar uma ResponseEntity personalizada que será enviada ao cliente da requisição HTTP. 
	 * 
	 * @param chave = representa uma chave que contem uma mensagem armazena no arquivo messages.properties
	 * @param exception = recebe uma exception que será capturada e tratada por ResponseEntityExceptionHadler  
	 * @param status = código de status da responsta HTTP que será enviado ao cliente (usuário)
	 * @param request = requisição web enviada pelo cliente (usuário)
	 * */
	public ResponseEntity<Object> criarResponseEntity(String chave, Exception exception, HttpStatus status,
			WebRequest request);

}
