package br.com.algamoneyapi.exceptionhandler.factory;

import org.apache.commons.lang3.exception.ExceptionUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.algamoneyapi.exceptionhandler.Erro;

/**
 * Cria uma ResponseEntity personalizada para enviar aos clientes das requisições HTTP,
 * caso uma Exception do tipo ResponseEntityExceptionHandler aconteça.
 * 
 * @see Personalizavel
 * @see ResponseEntityPersonalizada
 * 
 * @author Yuri Ramalho pinheiro
 * */

public class ResponseEntityPersonalizada extends ResponseEntityExceptionHandler implements Personalizavel {

	@Autowired
	private MessageSource messageSource;

	private String mensagemParaDesenvolvedor;

	private String mensagemParaUsuario;

	private Erro mensagemDeErro;

	/**
	 * ResponseEntity que será enviada ao cliente da requisição HTTP caso uma exception do
	 * ResponseEntityExceptionHandler seja capturada.
	 * 
	 * @see Personalizavel
	 * @see Erro
	 */
	@Override
	public ResponseEntity<Object> criarResponseEntity(String chave, Exception exception, HttpStatus status,
			WebRequest request) {

		// Lê o arquivo de propriedades messages.properties e recupera uma mensagem de
		// acordo com a chave passada.
		this.mensagemParaUsuario = messageSource.getMessage(chave, null, LocaleContextHolder.getLocale());

		// recupra a causa raiz da exception lançada
		this.mensagemParaDesenvolvedor = ExceptionUtils.getRootCauseMessage(exception);

		// prepara a mensagem de erro que será envida ao cliente da requisição
		this.mensagemDeErro = new Erro(this.mensagemParaUsuario, this.mensagemParaDesenvolvedor);

		// retorna uma ResponseEntity personalizada para o cliente requsição HTTP
		return handleExceptionInternal(exception, this.mensagemDeErro, new HttpHeaders(), status, request);
	}

}
