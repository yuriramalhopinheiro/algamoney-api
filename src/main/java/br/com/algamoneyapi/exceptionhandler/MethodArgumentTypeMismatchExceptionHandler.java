package br.com.algamoneyapi.exceptionhandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.method.annotation.MethodArgumentTypeMismatchException;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.algamoneyapi.exceptionhandler.factory.Personalizavel;

@ControllerAdvice
public class MethodArgumentTypeMismatchExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private Personalizavel personalizavel;

	@ExceptionHandler(MethodArgumentTypeMismatchException.class)
	protected ResponseEntity<Object> HandleMethodArgumentTypeMismatchException(MethodArgumentTypeMismatchException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		return this.personalizavel.criarResponseEntity("recuro.nao-encontrado", ex, HttpStatus.BAD_REQUEST, request);
	}

}
