package br.com.algamoneyapi.exceptionhandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;

import br.com.algamoneyapi.exceptionhandler.factory.Personalizavel;

/**
 * Envia uma mensagem de erro customizada para o usuário ou desenvolvedor quando o cliente 
 * tentar deletar um recuros, mas ele existe ou não é encontrado.
 * 
 *  @see MethodArgumentNotValidExceptionHandler
 *  @see BeanFactory
 *  @see Personalizavel
 *  
 *  @author Yuri Ramalho pinheiro
 */

@ControllerAdvice
public class EmptyResultDataAccessExceptionHandler extends MethodArgumentNotValidExceptionHandler{
	
	@Autowired
	private Personalizavel personalizavel;
	
	@ExceptionHandler({EmptyResultDataAccessException.class})
	public final ResponseEntity<Object> handleEmptyResultDataAccessException(EmptyResultDataAccessException ex, WebRequest request) {
		
		return this.personalizavel.criarResponseEntity("recuro.nao-encontrado", ex, HttpStatus.NOT_FOUND, request);
	}

}
