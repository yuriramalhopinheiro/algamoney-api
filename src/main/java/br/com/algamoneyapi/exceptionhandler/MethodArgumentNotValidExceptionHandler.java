package br.com.algamoneyapi.exceptionhandler;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.MessageSource;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

/**
 * Envia uma mensagem de erro customizada para o usuário ou desenvolvedor quando
 * a API receber uma requisção com valores dos campos(atributos) inválidos.
 * 
 * @author Yuri Ramalho pinheiro
 */
@ControllerAdvice
public class MethodArgumentNotValidExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private MessageSource messageSource;

	@Override
	protected final ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
			HttpHeaders headers, HttpStatus status, WebRequest request) {

		// cria uma lista de erros, para armazenar os campos inválidos, passados pelo
		// usuário
		List<Erro> erros = criarListaDeErros(ex.getBindingResult());

		// retorna uma resposta personalizada para o usuário
		return handleExceptionInternal(ex, erros, headers, status, request);
	}

	/**
	 * Cria uma lista para armazernar os campos com erro que foram enviados pelo
	 * usuário
	 */
	private List<Erro> criarListaDeErros(BindingResult bidingResult) {

		List<Erro> erros = new ArrayList<>();

		// recupera todos os campos com erro
		for (FieldError fieldError : bidingResult.getFieldErrors()) {

			// define a mesagem que será passada para o usuário e desenvolvedors
			String mensagemUsuario = messageSource.getMessage(fieldError, LocaleContextHolder.getLocale());

			String mensagemDesenvolvedor = fieldError.toString();

			Erro erro = new Erro(mensagemUsuario, mensagemDesenvolvedor);

			// adiciona o erro na lista de erros
			erros.add(erro);
		}

		return erros;
	}

}
