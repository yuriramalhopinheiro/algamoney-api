package br.com.algamoneyapi.exceptionhandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.algamoneyapi.exceptionhandler.factory.Personalizavel;
import br.com.algamoneyapi.service.exception.PessoaInvalidaOuInativaException;

/**
 * Envia uma mensagem de erro customizada para o usuário ou desenvolvedor quando o cliente 
 * tentar criar um lançamento que possua uma pessoa inválida ou inativa. 
 *
 * @see MethodArgumentNotValidExceptionHandler
 * @see BeanFactory
 * @see Personalizavel
 * 
 * @author Yuri Ramalho pinheiro
* */

@ControllerAdvice
public class PessoaInvalidaOuInativaExceptionHandler extends ResponseEntityExceptionHandler{
	
	@Autowired
	private Personalizavel personalizavel;
	
	@ExceptionHandler({PessoaInvalidaOuInativaException.class})
	public final ResponseEntity<Object> handlePessoaInvalidaOuInativaException(PessoaInvalidaOuInativaException ex){
		
		return this.personalizavel.criarResponseEntity("recurso.pessoa-invalida-ou-inativa", ex, HttpStatus.NOT_FOUND, null);
	}
	
}
