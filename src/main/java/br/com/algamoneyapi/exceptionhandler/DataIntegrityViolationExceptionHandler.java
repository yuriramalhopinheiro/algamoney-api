package br.com.algamoneyapi.exceptionhandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataIntegrityViolationException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.algamoneyapi.exceptionhandler.factory.Personalizavel;

/**
 * Envia uma mensagem de erro customizada para o usuário ou desenvolvedor quando o 
 * cliente tentar violar alguma regra de integridade do banco de dados
 *  
 * @see MethodArgumentNotValidExceptionHandler
 * @see BeanFactory
 * @see Personalizavel
 * 
 * @author Yuri Ramalho pinheiro
 **/

@ControllerAdvice
public class DataIntegrityViolationExceptionHandler extends ResponseEntityExceptionHandler {
	
	@Autowired
	private Personalizavel personalizavel;
	
	@ExceptionHandler({DataIntegrityViolationException.class})
	public final ResponseEntity<Object> handleDataIntegrityViolationException(DataIntegrityViolationException ex, WebRequest request){
		
		return this.personalizavel.criarResponseEntity("recurso.operacao-nao-perimitida", ex, HttpStatus.BAD_REQUEST, request);
	}

}
