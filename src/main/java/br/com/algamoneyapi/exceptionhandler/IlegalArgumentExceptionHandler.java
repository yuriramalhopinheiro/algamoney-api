package br.com.algamoneyapi.exceptionhandler;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import br.com.algamoneyapi.exceptionhandler.factory.Personalizavel;

/**
 * Envia uma mensagem de erro customizada para o usuário ou desenvolvedor quando
 * a API receber uma requisção com o código ou outro valor inválido ou nulo.
 * 
 * @see MethodArgumentNotValidExceptionHandler
 * @see BeanFactory
 * @see Personalizavel
 * 
 * @author Yuri Ramalho pinheiro
 */

@ControllerAdvice
public class IlegalArgumentExceptionHandler extends ResponseEntityExceptionHandler {

	@Autowired
	private Personalizavel personalizavel;
	
	@ExceptionHandler({ IllegalArgumentException.class })
	public final ResponseEntity<Object> handleIllegalArgumentException(IllegalArgumentException ex, WebRequest request) {

		return this.personalizavel.criarResponseEntity("recurso.valores-invalidos", ex, HttpStatus.NOT_ACCEPTABLE, request);
	}

}
