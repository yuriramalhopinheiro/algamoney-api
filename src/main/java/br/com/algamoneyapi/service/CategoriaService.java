package br.com.algamoneyapi.service;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import br.com.algamoneyapi.model.Categoria;
import br.com.algamoneyapi.repository.CategoriaRepository;

@Service
public class CategoriaService {

	@Autowired
	private CategoriaRepository repository;

	public List<Categoria> listar() {

		return this.repository.findAll();
	}

	public Categoria buscarPeloCodigo(Long codigo) {
	
		return (codigo != null ? this.repository.findOne(codigo) : null);
	}
	
	public Categoria criar(Categoria categoria) {

		return this.repository.save(categoria);
	}
}
