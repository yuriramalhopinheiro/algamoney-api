package br.com.algamoneyapi.service.exception;

/**
 * Caso uma Pessoa esteja com o status inativo está exception pode ser 
 * lançada. 
 * */
public class PessoaInvalidaOuInativaException extends RuntimeException {

	private static final long serialVersionUID = 1L;

}
