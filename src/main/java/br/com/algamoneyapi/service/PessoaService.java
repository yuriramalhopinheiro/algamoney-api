package br.com.algamoneyapi.service;

import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.stereotype.Service;

import br.com.algamoneyapi.model.Pessoa;
import br.com.algamoneyapi.repository.PessoaRepository;

@Service
public class PessoaService {

	@Autowired
	private PessoaRepository pessoaRepository;

	public List<Pessoa> listar() {

		return this.pessoaRepository.findAll();
	}
	
	public List<Pessoa> buscarPeloNome(String nome){
		
		return this.pessoaRepository.findByNomeContaining(nome);
	}
	
	public Pessoa buscarPeloCodigo(Long codigo) {

		Pessoa pessoaEncontrada = this.pessoaRepository.findOne(codigo);

		// caso o ojeto for null, esta exceção será lançada e captura por
		// AlgamoneyExceptionHandler
		if (pessoaEncontrada == null) {
			throw new EmptyResultDataAccessException(1);
		}

		return pessoaEncontrada;
	}

	public Pessoa atualiziar(Long codigo, Pessoa pessoa) {

		Pessoa pessoaEncontrada = buscarPeloCodigo(codigo);

		// copia os valores de um objeto par o outro, ignorando apenas o campo 'codigo'
		BeanUtils.copyProperties(pessoa, pessoaEncontrada, "codigo");

		this.pessoaRepository.save(pessoaEncontrada);
		
		return pessoaEncontrada;
	}

	public void atualizarPropriedadeAtivo(Long codigo, boolean ativo) {

		Pessoa pessoa = buscarPeloCodigo(codigo);
		pessoa.setAtivo(ativo);

		this.pessoaRepository.save(pessoa);
	}

	public Pessoa criar(Pessoa pessoa) {

		return this.pessoaRepository.save(pessoa);
	}

	public void remover(Long codigo) {

		Pessoa pessoa = this.buscarPeloCodigo(codigo);
		
		this.pessoaRepository.delete(pessoa.getCodigo());
	}
	
}
