package br.com.algamoneyapi.service;

import java.math.BigDecimal;
import java.time.LocalDate;
import java.time.format.DateTimeFormatter;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;

import br.com.algamoneyapi.model.Lancamento;
import br.com.algamoneyapi.model.Pessoa;
import br.com.algamoneyapi.model.filter.LancamentoFilter;
import br.com.algamoneyapi.model.filter.ResumoDoLancamento;
import br.com.algamoneyapi.repository.LancamentoRepository;
import br.com.algamoneyapi.repository.PessoaRepository;
import br.com.algamoneyapi.service.exception.PessoaInvalidaOuInativaException;

@Service
public class LancamentoService {

	@Autowired
	private LancamentoRepository lancamentoRepository;

	@Autowired
	private PessoaRepository pessoaRepository;

	public Page<Lancamento> listar(Pageable pageable) {

		return this.lancamentoRepository.findAll(pageable);
	}

	public List<Lancamento> pesquisarPelaDataDeVencimento(String dataVencimento) {

		LocalDate data = validarData(dataVencimento);

		return this.lancamentoRepository.findByDataVencimento(data);
	}

	private LocalDate validarData(String data) {

		try {
			DateTimeFormatter pattern = DateTimeFormatter.ofPattern("yyyy-MM-dd");

			return LocalDate.parse(data, pattern);

		} catch (DateTimeParseException ex) {
			throw new IllegalArgumentException("A data passada é inválida!");
		}

	}

	public List<Lancamento> pesquisarPelaDescricaoOuDataDeVencimento(LancamentoFilter lancamentoFilter) {

		LocalDate de = lancamentoFilter.getDataVencimentoDe();
		LocalDate ate = lancamentoFilter.getDataVencimentoAte();

		String descricao = lancamentoFilter.getDescricao();

		if (descricao != null && de != null && ate != null) {
			return this.lancamentoRepository.findByDescricaoContainingOrDataVencimentoBetween(descricao, de, ate);
		}

		if (de != null && ate != null) {
			return this.lancamentoRepository.findByDataVencimentoBetween(de, ate);
		}

		if (descricao != null) {
			return this.lancamentoRepository.findByDescricaoContaining(descricao);
		}

		return null;
	}

	public List<ResumoDoLancamento> pesquisarPelaDescricao(String descricao) {

		List<ResumoDoLancamento> lancamentos = new ArrayList<>();

		List<Object[]> lancamentosEncontratos = this.lancamentoRepository.pesquisarPelaDescricao(descricao);

		for (Object[] lancamento : lancamentosEncontratos) {

			ResumoDoLancamento resumoDoLancamento = new ResumoDoLancamento();
			resumoDoLancamento.setCodigo((Long) lancamento[0]);
			resumoDoLancamento.setDescricao((String) lancamento[1]);
			resumoDoLancamento.setValor((BigDecimal) lancamento[2]);
			resumoDoLancamento.setPessoa((String) lancamento[3]);

			lancamentos.add(resumoDoLancamento);
		}

		return lancamentos;
	}

	public Lancamento buscarPeloCodigo(Long codigo) {

		Lancamento lancamentoEncontrato = this.lancamentoRepository.findOne(codigo);

		// caso o objeto for null, esta exceção será lançada e capturada por
		// AlgamoneyExceptionHandler
		if (lancamentoEncontrato == null) {
			throw new EmptyResultDataAccessException(1);
		}

		return lancamentoEncontrato;
	}

	public Lancamento criar(Lancamento lancamento) {

		Pessoa pessoa = lancamento.getPessoa();

		pessoa = this.pessoaRepository.findOne(pessoa.getCodigo());

		// caso o objeto for null ou possuir o status inatitvo, esta exceção
		// será lançada e capturada por AlgamoneyExceptionHandler
		if (pessoa == null || pessoa.isInativo()) {
			throw new PessoaInvalidaOuInativaException();
		}

		return this.lancamentoRepository.save(lancamento);
	}

	public void remover(Long codigo) {

		Lancamento lancamento = this.buscarPeloCodigo(codigo);

		this.lancamentoRepository.delete(lancamento.getCodigo());
	}

	public Lancamento atualizar(Long codigo, Lancamento lancamento) {

		Lancamento lancamentoSalvo = this.lancamentoRepository.findOne(codigo);

		if (!lancamento.getPessoa().equals(lancamentoSalvo.getPessoa())) {

			validarPessoa(lancamento);
		}
		
		BeanUtils.copyProperties(lancamento, lancamentoSalvo, "codigo");
		
		return lancamentoRepository.save(lancamentoSalvo);
	}

	private void validarPessoa(Lancamento lancamento) {

		Pessoa pessoa = null;

		if (lancamento.getPessoa().getCodigo() != null) {

			pessoa = pessoaRepository.findOne(lancamento.getPessoa().getCodigo());
		}

		if (pessoa == null || pessoa.isInativo()) {

			throw new PessoaInvalidaOuInativaException();
		}

	}
}
