package br.com.algamoneyapi.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.algamoneyapi.model.Pessoa;
import br.com.algamoneyapi.resource.event.RecursoCriadoEvent;
import br.com.algamoneyapi.service.PessoaService;

@RestController
@RequestMapping("/pessoas")
public class PessoaResource {

	@Autowired
	private PessoaService pessoaService;

	@Autowired
	private ApplicationEventPublisher publisher;

	@GetMapping
	@PreAuthorize(value = "hasAuthority('ROLE_PESQUISAR_PESSOA') and #oauth2.hasScope('read')")
	public ResponseEntity<List<Pessoa>> listar() {

		List<Pessoa> pessoas = this.pessoaService.listar();
		
		// retorna uma responsta com status OK ou NO CONTENT
		return (pessoas.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(pessoas));
	}
	
	@GetMapping(params = "nome")
	@PreAuthorize(value = "hasAuthority('ROLE_PESQUISAR_PESSOA') and #oauth2.hasScope('read')")
	public ResponseEntity<List<Pessoa>> buscarPeloNome(String nome){
		
		List<Pessoa> pessoas = this.pessoaService.buscarPeloNome(nome);
		
		return pessoas.isEmpty() ? ResponseEntity.notFound().build() : ResponseEntity.ok(pessoas);
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize(value = "hasAuthority('ROLE_PESQUISAR_PESSOA') and #oauth2.hasScope('read')")
	public ResponseEntity<Pessoa> buscarPeloCodigo(@Valid @PathVariable Long codigo) {

		Pessoa pessoa = this.pessoaService.buscarPeloCodigo(codigo);

		return ResponseEntity.ok().body(pessoa);
	}

	@PostMapping
	@PreAuthorize(value = "hasAuthority('ROLE_CADASTRAR_PESSOA') and #oauth2.hasScope('write')")
	public ResponseEntity<Pessoa> criar(@Valid @RequestBody Pessoa pessoa, HttpServletResponse resposta) {

		Pessoa pessoaSalva = this.pessoaService.criar(pessoa);

		// publica um evento para informar que este recurso foi criado
		this.publisher.publishEvent(new RecursoCriadoEvent(this, resposta, pessoaSalva.getCodigo()));

		// retorna o recurso criado e a URI de localização
		return ResponseEntity.status(HttpStatus.CREATED).body(pessoaSalva);
	}

	@DeleteMapping("/{codigo}")
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	@PreAuthorize(value = "hasAuthority('ROLE_REMOVER_PESSOA') and #oauth2.hasScope('write')")
	public void remover(@PathVariable Long codigo) {

		this.pessoaService.remover(codigo);
	}

	@PutMapping("/{codigo}")
	@PreAuthorize(value = "hasAuthority('ROLE_CADASTRAR_PESSOA') and #oauth2.hasScope('write')")
	public ResponseEntity<Pessoa> atualizar(@PathVariable Long codigo, @Valid @RequestBody Pessoa pessoa) {

		Pessoa pessoaAtualizada = this.pessoaService.atualiziar(codigo, pessoa);

		return ResponseEntity.ok(pessoaAtualizada);
	}

	@PutMapping("/{codigo}/ativo")
	@ResponseStatus(HttpStatus.NO_CONTENT)
	@PreAuthorize(value = "hasAuthority('ROLE_CADASTRAR_PESSOA') and #oauth2.hasScope('write')")
	public void atualizarPropriedadeAtivo(@PathVariable Long codigo, @RequestBody boolean ativo) {

		this.pessoaService.atualizarPropriedadeAtivo(codigo, ativo);
	}
	
}
