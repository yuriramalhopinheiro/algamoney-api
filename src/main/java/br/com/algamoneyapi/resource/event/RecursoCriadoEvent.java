package br.com.algamoneyapi.resource.event;

import java.net.URI;

import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEvent;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

/**
 * Este evento será chamando quando um recurso for criado pelo cliente.
 * */
public class RecursoCriadoEvent extends ApplicationEvent{

	private static final long serialVersionUID = 1L;
	
	private HttpServletResponse response;
	private Long codigo;
	
	
	public RecursoCriadoEvent(Object source, HttpServletResponse response, Long codigo) {
		super(source);
		this.response = response;
		this.codigo = codigo;
	}
	
	/**
	 * Personaliza a resposta HTTP que será enviada ao cliente.
	 * */
	public void pesonalizarRespostaHTTP() {
		
		//cria uma nova URI para identificar o recurso criado
		URI uri = ServletUriComponentsBuilder.fromCurrentRequestUri().path("/{codigo}")
				.buildAndExpand(codigo).toUri();
		
		//adiciona no cabeçalho de resposta, a URI criada
		response.setHeader("Location", uri.toASCIIString());
	}
	
	
}
