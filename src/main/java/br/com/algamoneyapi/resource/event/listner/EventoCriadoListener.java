package br.com.algamoneyapi.resource.event.listner;

import org.springframework.context.ApplicationListener;
import org.springframework.stereotype.Component;

import br.com.algamoneyapi.resource.event.RecursoCriadoEvent;

/**
 * Sempre que um evento do tipo RecursoCriadoEvent ocorrer dentro da aplicação, 
 * o método onApplicationEvent será chamado para executar alguma ação.
 * */
@Component
public class EventoCriadoListener implements ApplicationListener<RecursoCriadoEvent>{

	@Override
	public void onApplicationEvent(RecursoCriadoEvent recursoCriadoEvent) {
		
		recursoCriadoEvent.pesonalizarRespostaHTTP();
	}

}
 