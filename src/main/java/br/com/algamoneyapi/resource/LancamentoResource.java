package br.com.algamoneyapi.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;

import br.com.algamoneyapi.model.Lancamento;
import br.com.algamoneyapi.model.filter.LancamentoFilter;
import br.com.algamoneyapi.model.filter.ResumoDoLancamento;
import br.com.algamoneyapi.resource.event.RecursoCriadoEvent;
import br.com.algamoneyapi.service.LancamentoService;

@RestController
@RequestMapping("/lancamentos")
public class LancamentoResource {

	@Autowired
	private LancamentoService lancamentoService;

	@Autowired
	private ApplicationEventPublisher publisher;

	@GetMapping
	@PreAuthorize(value = "hasAuthority('ROLE_PESQUISAR_LANCAMENTO') and #oauth2.hasScope('read')")
	public Page<Lancamento> buscarComPaginacao(Pageable pageable) {
		
		/*
		 * @param pageable define os parametros da paginação que será criada.
		 * Ao efetuar a requisição HTTP, informe os valores para os parâmetros 
		 * page e size.
		 * Exemplo: http://localhost:8080/lancamentos?size=4&page=1, 
		 * então será retornado 4 registros por página
		 * */
		return this.lancamentoService.listar(pageable);
	}
	
	@GetMapping(params = "resumo")
	@PreAuthorize(value = "hasAuthority('ROLE_PESQUISAR_LANCAMENTO') and #oauth2.hasScope('read')")
	public ResponseEntity<List<ResumoDoLancamento>> resumir(String descricao){
		
		List<ResumoDoLancamento> lancamentos = this.lancamentoService.pesquisarPelaDescricao(descricao);
		
		return (lancamentos.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(lancamentos));
	}
	
	@GetMapping("/por_descricao_ou_data_de_vencimento")
	@PreAuthorize(value = "hasAuthority('ROLE_PESQUISAR_LANCAMENTO') and #oauth2.hasScope('read')")
	public ResponseEntity<List<Lancamento>> buscarPelaDescricaoOuDataDeVencimento(LancamentoFilter lancamentoFilter){
		
		List<Lancamento> lancamentos = this.lancamentoService.pesquisarPelaDescricaoOuDataDeVencimento(lancamentoFilter);
		
		// retorna uma responsta com status OK ou NO CONTENT
		return (lancamentos.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(lancamentos));
	}
	
	@GetMapping("/{codigo}")
	@PreAuthorize(value = "hasAuthority('ROLE_PESQUISAR_LANCAMENTO') and #oauth2.hasScope('read')")
	public ResponseEntity<Lancamento> buscarPeloCodigo(@Valid @PathVariable Long codigo) {

		Lancamento lancamento = this.lancamentoService.buscarPeloCodigo(codigo);

		return ResponseEntity.ok(lancamento);
	}

	@PostMapping
	@PreAuthorize(value = "hasAuthority('ROLE_CADASTRAR_LANCAMENTO') and #oauth2.hasScope('write')")
	public ResponseEntity<?> criar(@RequestBody @Valid Lancamento lancamento, HttpServletResponse resposta) {
		
		Lancamento lacamentoCriado = this.lancamentoService.criar(lancamento);
		
		// publica um evento para informar que este recurso foi criado
		this.publisher.publishEvent(new RecursoCriadoEvent(this, resposta, lacamentoCriado.getCodigo()));
		
		// retorna o recurso criado e a URI de localização
		return ResponseEntity.status(HttpStatus.CREATED).body(lacamentoCriado);
	}	
	
	@DeleteMapping("/{codigo}")
	@ResponseStatus(code = HttpStatus.NO_CONTENT)
	@PreAuthorize(value = "hasAuthority('ROLE_REMOVER_LANCAMENTO') and #oauth2.hasScope('write')")
	public void remover(@PathVariable Long codigo) {
		
		this.lancamentoService.remover(codigo);
	}
	
	@PutMapping("/{codigo}")
	@PreAuthorize(value = "hasAuthority('ROLE_CADASTRAR_LANCAMENTO') and #oauth2.hasScope('write')")
	public ResponseEntity<Lancamento> atualizar(@PathVariable Long codigo, @RequestBody @Valid Lancamento lancamento){
		
		Lancamento lancamentoAtualizado = this.lancamentoService.atualizar(codigo, lancamento);
		
		return ResponseEntity.ok(lancamentoAtualizado);
	}
}
