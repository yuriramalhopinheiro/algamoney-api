package br.com.algamoneyapi.resource;

import javax.servlet.http.Cookie;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.algamoneyapi.config.property.AlgamoneyApiProperty;

/**
 * Recurso para invalidar o RefreshToken e encerrar o acesso do cliente à API
 * */

@RestController
@RequestMapping("/tokens")
public class TokenResource {
	
	@Autowired
	private AlgamoneyApiProperty algamoneyApiProperty;
	
	public void revoke(HttpServletRequest requisicao, HttpServletResponse resposta) {
		
		Cookie cookie = new Cookie("refreshToken", null);
		cookie.setHttpOnly(true);
		cookie.setSecure(this.algamoneyApiProperty.getSeguranca().isEnableHttps());
		cookie.setPath(requisicao.getContextPath() + "/oauth/token");
		cookie.setMaxAge(0);
		
		resposta.addCookie(cookie);
		resposta.setStatus(HttpStatus.NO_CONTENT.value());
	}
	
}
