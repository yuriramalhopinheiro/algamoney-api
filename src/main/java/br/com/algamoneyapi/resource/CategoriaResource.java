package br.com.algamoneyapi.resource;

import java.util.List;

import javax.servlet.http.HttpServletResponse;
import javax.validation.Valid;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.ApplicationEventPublisher;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import br.com.algamoneyapi.model.Categoria;
import br.com.algamoneyapi.resource.event.RecursoCriadoEvent;
import br.com.algamoneyapi.service.CategoriaService;

@RestController
@RequestMapping("/categorias")
public class CategoriaResource {

	@Autowired
	private CategoriaService categoriaService;

	@Autowired
	private ApplicationEventPublisher publisher;

	@GetMapping
	@PreAuthorize(value = "hasAuthority('ROLE_PESQUISAR_CATEGORIA') and #oauth2.hasScope('read') ")
	public ResponseEntity<List<Categoria>> listar() {

		List<Categoria> categorias = this.categoriaService.listar();
		
		// retorna uma responsta com status OK ou NO CONTENT
		return ( categorias.isEmpty() ? ResponseEntity.noContent().build() : ResponseEntity.ok(categorias));
	}

	@GetMapping("/{codigo}")
	@PreAuthorize(value = "hasAuthority('ROLE_PESQUISAR_CATEGORIA') and #oauth2.hasScope('read') ")
	public ResponseEntity<Categoria> buscarPeloCodigo(@PathVariable Long codigo) {

		Categoria categoria = this.categoriaService.buscarPeloCodigo(codigo);

		// retorna uma resposta com estatus OK ou NOT FOUND
		return (categoria != null ? ResponseEntity.ok(categoria) : ResponseEntity.notFound().build());
	}

	@PostMapping
	@PreAuthorize(value = "hasAuthority('ROLE_CADASTRAR_CATEGORIA') and #oauth2.hasScope('write') ")
	public ResponseEntity<Categoria> criar(@Valid @RequestBody Categoria categoria, HttpServletResponse resposta) {

		Categoria categoriaSalva = this.categoriaService.criar(categoria);

		// publica um evento para informar que este recurso foi criado
		publisher.publishEvent(new RecursoCriadoEvent(this, resposta, categoria.getCodigo()));

		// retorna o recurso criado e a URI de localização
		return ResponseEntity.status(HttpStatus.CREATED).body(categoriaSalva);
	}

}
