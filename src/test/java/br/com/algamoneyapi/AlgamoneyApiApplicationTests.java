package br.com.algamoneyapi;

import static org.junit.Assert.assertTrue;

import java.math.BigDecimal;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import br.com.algamoneyapi.model.Pessoa;
import br.com.algamoneyapi.model.filter.ResumoDoLancamento;
import br.com.algamoneyapi.repository.LancamentoRepository;
import br.com.algamoneyapi.repository.PessoaRepository;

@RunWith(SpringRunner.class)
@SpringBootTest
public class AlgamoneyApiApplicationTests {
	
	@Autowired
	private LancamentoRepository repository;

	@Autowired
	private PessoaRepository pessoaRepository;
	
	@Test
	public void contextLoads() {

		//resumirLancamentos();
		//buscarPessoasPeloNome();
		comparar();
	}
	
	public void resumirLancamentos() {
		
		List<ResumoDoLancamento> lancamentos = new ArrayList<>();
		
		List<Object[]> lancamentosEncontratos  = this.repository.pesquisarPelaDescricao("lu");
		
		for(Object[] lancamento : lancamentosEncontratos) {
			
			ResumoDoLancamento resumoDoLancamento = new ResumoDoLancamento();
			resumoDoLancamento.setCodigo((Long) lancamento[0]);
			resumoDoLancamento.setDescricao( (String) lancamento[1]);
			resumoDoLancamento.setValor( (BigDecimal) lancamento[2]);
			resumoDoLancamento.setPessoa( (String) lancamento[3]);
			
			lancamentos.add(resumoDoLancamento);
		}
		
		for(ResumoDoLancamento resumo : lancamentos) {
			System.out.println(resumo.getDescricao());
			System.out.println(resumo.getValor());
		}
		
	}
	
	public void buscarPessoasPeloNome(){
		
		List<Pessoa> pessoas = this.pessoaRepository.findByNomeContaining("v");
		
		pessoas.forEach( p -> System.out.println(p.getNome()));
	}
	
	public void comparar() {
	
		assertTrue(5 < 10);
	}
	
}
